// console.log("test");
// console.log("chat");

// variable
// donne un nom à un quelque chose : on lui donne une valeur
// déclaration et assignation de valeur
let firstName = "Michel"; // const : constante, let : variable
// remplacé la valeur
firstName = "Gertrude";
// console.log(firstName);

// var vs let : var vieux JS, porté (scope) de var est supérieur
// effet de bord : impacte des choses à des endroits imprévus

// type de données en JavaScript
const string = "string";
const number = 1.5; // réel ou entier  : avec et sans virgules
const boolean = true;

// une fonction a le type function
const object = { name: "Michel", age: 50 };

// tableau
const array = [0, 1, 2];
// console.log(typeof array); // object

let undefinedVariable;
const nullVariable = null;
// console.log("type1", undefinedVariable, "type2", nullVariable);

// null vs undefined vs empty value
// undefined : pas de type défini ni de valeur, c'est pas bien d'avoir undefined
// null : on a choisi que pour le moment c'était vide
// empty value, ou valeur vide : "", 0, []

// const firstName = "Michel";
const lastName = "Durand";
const fullName = "Michel " + lastName; // concaténation : coller deux chaînes ensemble

// console.log(fullName); // "Michel Durand"

// inverser les valeurs des variables a et b
let a = 3;
let b = 5;
// a qui vaut 5 et b qui vaut 3 (sans taper les nombre 3 ou 5)
// mettre une valeur (soit a, soit b) de côté
let c = a ; // 3
a = b // 5
b = c
// a: 5 b: 5

// console.log("a:", a, "b:", b);

// condition
// embranchement
// firstName

console.log("0" == 0, "0" === 0); // true, false

// = (assignation, reçoit une valeur) ? == (la valeur) ? === : strictement égal (le type et la valeur)
if (firstName === "Michel") {
  console.log("bonjour");
} else {
  console.log("je te connais pas !");
}

// {} : brackets (curly braces) : accolades
// [] : (square brackets) : crochets
// () : parenthèses

// pour les chaînes caractères
// "" : double quotes : guillemets
// '' : single quotes : apostrophes
// `` : backticks : accents graves

const value = 3;
console.log("Ma valeur est " + value);
console.log('Ma valeur est ' + value);
console.log(`Ma valeur est ${value}`); // template string : interpolation

// demain : les fonctions ! et les boucles
// c'est quoi une fonction ?
// permet d'exécuter du code
// dépend des paramètres

// afficher vs retourner ?
// console.log() (debug) vs return

// déclarer une fonction un paramètre
function direBonjour(nom) { // "Bernard"
  // corps de la fonction : traitement
  let retour = "Bonjour " + nom;

  // renvoyer un résultat
  return retour;
}

// appel de fonction : passage d'argument (la valeur passée en paramètre)
const test = direBonjour("Bernard");
// console.log(test); // "Bonjour Bernard"
// console.log(direBonjour("Barbara"));

const randomValue = Math.random(); // un nombre aléatoire entre 0 et 0.9999999 (1, non compris)

// fonction soustraire qui a deux nombres en entrée, et qui soustrait le second nombre au premier
// 4, 3 -> (4 - 3 -> 1)
function soustraction(number1, number2) {
  let result = number1 - number2;
  // affichage
  // console.log(result);
  // retour
  return result;
}

// console.log(soustraction(4, 3));
// console.log(soustraction(48, 12));

// qu'est-ce -- ? ++ : incrémentation, ajouter un
let a = 0;
a++; // a = a + 1;
// console.log("a=", a)
a--;
// console.log("a=", a)

// qui prend un nom, et un prénom en paramètre, et qui renvoi "Prenom Nom"
function fullName(lastName, firstName) {
  let name = lastName + " " + firstName; // concaténation
  // let name = `${lastName} ${firstName}`; // interpolation

  return name;
}

const test1 = fullName("Nom de famille", "Prénom");
// console.log(test1);

function majeur(age) {
  // let majority; // undefined
  if (age >= 18) {
    // majority = true;
    return true;
  } else {
    // majority = false;
    return false;
  }
  // return majority;
}
const result = majeur(20);
console.log("20", result);
console.log("12", majeur(12));

// on passe un age, ça nous renvoi "vrai" si c'est une personne majeure, sinon ça renvoi "faux"
/*
function age() {
if(age <= 18)
console.log("vous êtes majeur")

else("vous êtes mineur")
}
*/